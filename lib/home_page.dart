import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyHomePage extends StatelessWidget {
  final String helloText;

  MyHomePage({Key key, this.helloText}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(helloText),
      ),
      body: Stack(
        children: <Widget>[
          Container(
            height: 200,
            width: 150,
            child: Center(child: Text("Bayy")),
            color: Colors.cyanAccent,
          ),
          Center(
            child: Container(
              height: 100,
              width: 50,
              child: Center(child: Text("Bayy")),
              color: Colors.amber,
            ),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          showDialog(
            context: context,
            child: AlertDialog(
              title: Text("onPress"),
              content: Text("content"),
            ),
            barrierDismissible: false,
          );
        },
      ),
    );
  }
}
